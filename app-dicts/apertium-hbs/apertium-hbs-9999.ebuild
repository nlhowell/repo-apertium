# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-lang

DESCRIPTION="Apertium monolingual module for Serbo-Croatian."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-3"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/hfst
	sci-misc/vislcg3
"
BDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/hfst
	sci-misc/vislcg3
	sys-apps/gawk
"
