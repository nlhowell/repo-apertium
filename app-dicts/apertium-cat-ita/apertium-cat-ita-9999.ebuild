# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-pair

DESCRIPTION="Apertium bilingual module for Catalan-Italian."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-2+"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
"
BDEPEND+="
	>=app-dicts/apertium-cat-2.8.0
	>=app-dicts/apertium-ita-0.10.5
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/vislcg3
	sys-apps/gawk
"
