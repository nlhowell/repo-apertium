# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-pair

DESCRIPTION="Apertium bilingual module for Esperanto-Russian."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-3+"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/vislcg3
"
BDEPEND+="
	app-dicts/apertium-epo
	app-dicts/apertium-rus
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/vislcg3
	sys-apps/gawk
"
