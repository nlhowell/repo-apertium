# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

GIELLA_EXP=( "ext-apertium" )

inherit giella-lang

DESCRIPTION="Giellatekno morphological dictionaries for K'iche'."

LICENSE="NOASSERTION"
SLOT="0"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="sci-misc/vislcg3"
