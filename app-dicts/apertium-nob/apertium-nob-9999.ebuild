# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-lang

DESCRIPTION="Apertium monolingual module for Norwegian Bokmål."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-2+"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/vislcg3
"
BDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/vislcg3
	sys-apps/gawk
"
