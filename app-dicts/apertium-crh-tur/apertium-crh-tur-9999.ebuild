# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-pair

DESCRIPTION="Apertium bilingual module for Crimean Tatar-Turkish."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-3+"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/hfst
	sci-misc/vislcg3
"
BDEPEND+="
	>=app-dicts/apertium-crh-0.2.0-r83161
	>=app-dicts/apertium-tur-0.2.0-r83161
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/hfst
	sci-misc/vislcg3
	sys-apps/gawk
"
