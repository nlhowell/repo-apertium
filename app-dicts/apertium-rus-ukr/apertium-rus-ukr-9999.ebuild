# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-pair

DESCRIPTION="Apertium bilingual module for Russian-Ukrainian."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-2+"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/vislcg3
"
BDEPEND+="
	>=app-dicts/apertium-rus-0.2.0-r82706
	>=app-dicts/apertium-ukr-0.1.0-r82563
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/vislcg3
	sys-apps/gawk
"
