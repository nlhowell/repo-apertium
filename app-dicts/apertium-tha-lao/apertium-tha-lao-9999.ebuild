# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-pair

DESCRIPTION="Apertium bilingual module for Thai-Lao."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-3+"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/vislcg3
"
BDEPEND+="
	app-dicts/apertium-lao
	app-dicts/apertium-tha
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/vislcg3
	sys-apps/gawk
"
