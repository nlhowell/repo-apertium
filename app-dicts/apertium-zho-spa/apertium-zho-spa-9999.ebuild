# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-pair

DESCRIPTION="Apertium bilingual module for Chinese-Spanish."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-3+"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
"
BDEPEND+="
	app-dicts/apertium-spa
	app-dicts/apertium-zho
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sys-apps/gawk
"
