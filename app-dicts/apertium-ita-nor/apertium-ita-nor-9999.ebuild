# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-pair

DESCRIPTION="Apertium bilingual module for Italian-Norwegian."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-3+"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
"
BDEPEND+="
	app-dicts/apertium-ita
	app-dicts/apertium-nno
	app-dicts/apertium-nob
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sys-apps/gawk
"
