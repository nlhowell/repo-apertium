# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-pair

DESCRIPTION="Apertium bilingual module for Spanish-Italian."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-2"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
"
BDEPEND+="
	>=app-dicts/apertium-ita-0.9.0-r78828
	>=app-dicts/apertium-spa-1.0.0-r78827
	>=sci-misc/apertium-3.6.0
	sys-apps/gawk
"
