# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

EAPI=7

inherit apertium-pair

DESCRIPTION="Apertium bilingual module for Norwegian Nynorsk-Norwegian Bokmål."
HOMEPAGE="https://apertium.org"

SLOT="0"

LICENSE="GPL-2+"
RDEPEND+="
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/vislcg3
"
BDEPEND+="
	>=app-dicts/apertium-nno-1.1.0
	>=app-dicts/apertium-nob-1.1.0
	>=sci-misc/apertium-3.6.0
	sci-misc/apertium-lex-tools
	sci-misc/vislcg3
	sys-apps/gawk
"
