# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

GIELLA_EXP=( "" )

inherit giella-lang

DESCRIPTION="Giellatekno morphological dictionaries for Latvian."

LICENSE="GPL-2"
SLOT="0"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="sci-misc/vislcg3"
