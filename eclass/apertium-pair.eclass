# Copyright 2020 Nick Howell <nlhowell@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: apertium-pair.eclass
# @MAINTAINER:
# Nick Howell <nlhowell@gmail.com>
# @AUTHOR:
# Nick Howell <nlhowell@gmail.com>
# @BLURB: Helper functions for Apertium bilingual ebuilds.
# @DESCRIPTION:
# Eclass with helper and phase functions for Apertium bilingual
# modules.

case "${EAPI:-0}" in
	0|1|2|3|4|5|6)
		die "Unsupported EAPI-${EAPI} for ${ECLASS} (obsolete)"
		;;
	7)
		;;
	*)
		die "Unsupported EAPI=${EAPI} for ${ECLASS} (unknown)"
		;;
esac

if [[ ! ${_APERTIUM_PAIR} ]]; then

inherit apertium

EXPORT_FUNCTIONS src_prepare src_install src_configure

# @ECLASS-VARIABLE: APERTIUM_PAIR_LANGPAIR
# @DESCRIPTION: Hyphenated SRC-TGT ISO-639-3 codes.
: ${APERTIUM_PAIR_LANGPAIR:=${PN#apertium-}}

# @ECLASS-VARIABLE: APERTIUM_PAIR_SLANG
# @DESCRIPTION: Source language ISO-639-3 code 
readonly APERTIUM_PAIR_SLANG=${APERTIUM_PAIR_LANGPAIR%-*}

# @ECLASS-VARIABLE: APERTIUM_PAIR_TLANG
# @DESCRIPTION: Target language ISO-639-3 code 
readonly APERTIUM_PAIR_TLANG=${APERTIUM_PAIR_LANGPAIR#*-}

# @ECLASS-VARIABLE: APERTIUM_PAIR_REDEPEND
# @DESCRIPTION: Runtime dependencies for Apertium monolingual
# packages.
readonly APERTIUM_PAIR_RDEPEND="
	sci-misc/apertium
"
RDEPEND+=" $APERTIUM_PAIR_RDEPEND"

# @ECLASS-VARIABLE: APERTIUM_PAIR_BDEPEND
# @DESCRIPTION: Build dependencies for Apertium monolingual packages.
readonly APERTIUM_PAIR_BDEPEND="
	virtual/pkgconfig
	sci-misc/apertium
	sci-misc/lttoolbox
"
BDEPEND+=" $APERTIUM_PAIR_BDEPEND"

# @ECLASS-VARIABLE: APERTIUM_PAIR_DEPEND
# @DESCRIPTION: Build-time target dependencies for Apertium
# monolingual packages.
readonly APERTIUM_PAIR_DEPEND="
"
DEPEND+=" $APERTIUM_PAIR_DEPEND"

# @FUNCTION: apertium-pair_src_prepare
# @USAGE:
# @DESCRIPTION:
# Common src_prepare for Apertium bilingual modules.
apertium-pair_src_prepare() {
	default
	apertium-pair_src-qa-check
	eautoreconf
}

# @FUNCTION: apertium-pair_src-qa
# @USAGE: 
# @DESCRIPTION: Perform QA checks on source code.
apertium-pair_src-qa-check() {
	:
}

# @FUNCTION: apertium-pair_src_install
# @USAGE:
# @DESCRIPTION: 
# Common src_install for Apertium bilingual modules.
apertium-pair_src_install() {
	default
	apertium-pair_install-qa-checks
}

# @FUNCTION: apertium-pair_install-qa-checks
# @USAGE:
# @DESCRIPTION:
# Perform QA checks on install image.
apertium-pair_install-qa-checks() {
	apertium_install-qa-check-modes
}

# @FUNCTION: apertium-pair_conf
# @USAGE: [econf args]
# @DESCRIPTION: Run econf with apertium-pair use_enable's
apertium-pair_conf() {
	econf $@
}

# @FUNCTION: apertium-pair_src_configure
# @USAGE:
# @DESCRIPTION: 
# Default src_configure for Apertium bilingual modules; just run
# apertium-lang-conf
apertium-pair_src_configure() {
	apertium-pair_conf
}

_APERTIUM_PAIR=1
fi

