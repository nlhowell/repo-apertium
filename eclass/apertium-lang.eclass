# Copyright 2020 Nick Howell <nlhowell@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: apertium-lang.eclass
# @MAINTAINER:
# Nick Howell <nlhowell@gmail.com>
# @AUTHOR:
# Nick Howell <nlhowell@gmail.com>
# @BLURB: Helper functions for Apertium monolingual ebuilds.
# @DESCRIPTION:
# Eclass with helper and phase functions for Apertium monolingual
# modules.

case "${EAPI:-0}" in
	0|1|2|3|4|5|6)
		die "Unsupported EAPI-${EAPI} for ${ECLASS} (obsolete)"
		;;
	7)
		;;
	*)
		die "Unsupported EAPI=${EAPI} for ${ECLASS} (unknown)"
		;;
esac

if [[ ! ${_APERTIUM_LANG} ]]; then

inherit apertium

EXPORT_FUNCTIONS src_prepare src_install src_configure

IUSE+=" ospell"
# @ECLASS-VARIABLE: APERTIUM_LANG_REDEPEND
# @DESCRIPTION: Runtime dependencies for Apertium monolingual
# packages.
readonly APERTIUM_LANG_RDEPEND="
	sci-misc/apertium
"
RDEPEND+=" $APERTIUM_LANG_RDEPEND"

# @ECLASS-VARIABLE: APERTIUM_LANG_BDEPEND
# @DESCRIPTION: Build dependencies for Apertium monolingual packages.
readonly APERTIUM_LANG_BDEPEND="
	virtual/pkgconfig
	sci-misc/apertium
	sci-misc/lttoolbox
	ospell? (
		app-arch/zip
		sci-libs/giella-core
	)
"
BDEPEND+=" $APERTIUM_LANG_BDEPEND"

# @ECLASS-VARIABLE: APERTIUM_LANG_DEPEND
# @DESCRIPTION: Build-time target dependencies for Apertium
# monolingual packages.
readonly APERTIUM_LANG_DEPEND="
"
DEPEND+=" $APERTIUM_LANG_DEPEND"

# @ECLASS-VARIABLE: APERTIUM_LANG_ISO
# @DESCRIPTION: Module ISO-639-3 language code 
: ${APERTIUM_LANG_ISO:=${PN#apertium-}}

# @ECLASS-VARIABLE: APERTIUM_LANG_IETFs
# @REQUIRED
# @DESCRIPTION:
# Module IETF BCP-47 language codes.

# @FUNCTION: apertium-lang_src_prepare
# @USAGE:
# @DESCRIPTION:
# Common src_prepare for Apertium monolingual modules.
apertium-lang_src_prepare() {
	default
	apertium-lang_src-qa-check-spellers
	eautoreconf
}

# @FUNCTION: apertium-lang_src-qa
# @USAGE: 
# @DESCRIPTION: Perform QA checks on source code.
apertium-lang_src-qa-check() {
	apertium-lang_src-qa-check-spellers
}

# @FUNCTION: apertium-lang_src-qa-check-spellers
# @USAGE:
# @DESCRIPTION: Perform speller QA checks on source code.
apertium-lang_src-qa-check-spellers() {
	if ! grep -q zhfst Makefile.am; then
		if use ospell; then
			eerror "no zhfst built in Makefile.am, but USE=ospell"
		else
			eqawarn "no zhfst build support in Makefile.am"
		fi
	fi
}

# @FUNCTION: apertium-lang_src_install
# @USAGE:
# @DESCRIPTION: 
# Common src_install for Apertium monolingual modules.
apertium-lang_src_install() {
	default
	apertium-lang_install-qa-checks
}

# @FUNCTION: apertium-lang_install-qa-checks
# @USAGE:
# @DESCRIPTION:
# Perform QA checks on install image.
apertium-lang_install-qa-checks() {
	apertium-lang_install-qa-check-spellers
	apertium_install-qa-check-modes
}


# @FUNCTION: apertium-lang_install-qa-check-spellers
# @USAGE:
# @DESCRIPTION: Perform speller QA checks on install.
apertium-lang_install-qa-check-spellers() {
	exists-any() {
		path="$1"
		shift
		for speller in $@; do
			test -f "$ED/usr/share/$path/$speller.zhfst" &&
			return 0
		done
		return 1
	}
	exists-all() {
		path="$1"
		shift
		for speller in $@; do
			test -f "$ED/usr/share/$path/$speller.zhfst" ||
			return 1
		done
		return 0
	}
	local apertium="apertium/$PN"
	local voikko="voikko/3"
	if use ospell; then
		exists-all "$apertium" $APERTIUM_LANG_ISO || eqawarn "USE=ospell but Apertium speller not installed."
		exists-any "$voikko" ${APERTIUM_LANG_IETFs[@]} || eqawarn "USE=ospell but Voikko spellers not installed."
		exists-all "$voikko" ${APERTIUM_LANG_IETFs[@]} || eqawarn "USE=ospell but not all Voikko spellers installed."
	else
		exists-any "$apertium" $APERTIUM_LANG_ISO && eqawarn "USE=-ospell but Apertium speller installed."
		exists-any "$voikko" ${APERTIUM_LANG_IETFs[@]} && eqawarn "USE=-ospell but Voikko speller installed."
	fi
	unset apertium voiko
}

# @FUNCTION: apertium-lang_conf
# @USAGE: [econf args]
# @DESCRIPTION: Run econf with apertium-lang use_enable's
apertium-lang_conf() {
	econf "$(use_enable ospell)" $@
}

# @FUNCTION: apertium-lang_src_configure
# @USAGE:
# @DESCRIPTION: 
# Default src_configure for Apertium monolingual modules; just run
# apertium-lang_conf
apertium-lang_src_configure() {
	apertium-lang_conf
}

_APERTIUM_LANG=1
fi

