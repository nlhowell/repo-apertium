# Copyright 2020 Nick Howell <nlhowell@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: apertium.eclass
# @MAINTAINER:
# Nick Howell <nlhowell@gmail.com>
# @AUTHOR:
# Nick Howell <nlhowell@gmail.com>
# @BLURB: Helper functions for Apertium ebuilds.
# @DESCRIPTION:
# Eclass for handling build requirements of Apertium language modules.

case "${EAPI:-0}" in
	0|1|2|3|4|5|6)
		die "Unsupported EAPI-${EAPI} for ${ECLASS} (obsolete)"
		;;
	7)
		;;
	*)
		die "Unsupported EAPI=${EAPI} for ${ECLASS} (unknown)"
		;;
esac

if [[ ! ${_APERTIUM} ]]; then

inherit autotools
if [[ ${PV} = *9999* ]]; then
	: ${EGIT_REPO_URI:=https://github.com/apertium/$PN}
	inherit git-r3
	: ${SRC_URI:=}
	: ${KEYWORDS:=}
else
	: ${SRC_URI:="https://github.com/apertium/${PN}/archive/tags/v${PV}.tar.gz -> ${P}.tar.gz"}
	: ${KEYWORDS:=~amd64 ~arm ~x86}
	EXPORT_FUNCTIONS src_unpack
fi

# @FUNCTION: apertium-rename-workdir
# @USAGE:
# @DESCRIPTION:
# Rename work directory for unpacked github tarballs.
apertium_rename-workdir() {
  [[ ${PV} = *9999* ]] && return 0
  mv "${WORKDIR}/apertium-${PN}-"* "${WORKDIR}/${P}"
}

# @FUNCTION: apertium-src_unpack
# @USAGE:
# @DESCRIPTION:
# Default src_unpack for Apertium linguistic modules.
apertium_src_unpack() {
  default
  apertium_rename-workdir
}

# @FUNCTION: apertium_install-qa-check-modes
# @USAGE: 
# @DESCRIPTION:
# QA check that all declared transducers are installed, and that no
# undeclared transducers are installed.
apertium_install-qa-check-modes() {
	exists-all() {
		result=0
		for a in $@; do
			if [[ ! -f "$ED/usr/share/apertium/$PN/$a" ]]; then
				eqawarn "Failed to find '/usr/share/apertium/$PN/$a' in install image."
				result=1
			fi
		done
		return $result
	}
	[[ -f "${S}/modes.xml" ]] || die "QA: No modes.xml file."
	XSLTPROC=$EPREFIX/usr/bin/xsltproc
	$XSLTPROC <(cat <<EOF
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<xsl:for-each select="modes/mode[@install='yes']">
<xsl:value-of select="./@name" />:<xsl:for-each select="pipeline/program/file"><xsl:value-of select="./@name"/>,</xsl:for-each>:
</xsl:for-each>
</xsl:template>

</xsl:stylesheet>
EOF
) "${S}/modes.xml" \
	| grep -v '<?xml' \
	| while read line; do
		mode="${line%%:*}"
		line="${line#${mode}:}"
		files="${line%:}"
		exists-all ${files//,/ } || eerror "QA: missing files required for mode $mode."
	done
}

_APERTIUM=1

fi

