# Copyright 2020 Nick Howell
# Distributed under the terms of the GNU General Public License v3

# @ECLASS: giella-lang.eclass
# @MAINTAINER:
# Nick Howell <nlhowell@gmail.com>
# @AUTHOR:
# Nick Howell <nlhowell@gmail.com>
# @BLURB: Eclass for giellalt morphological dictionaries.
# @DESCRIPTION:
# Eclass for handling build requirements of giellalt morphological
# dictionaries.

case "${EAPI:-0}" in
	0|1|2|3|4|5|6)
		die "Unsupported EAPI=${EAPI} for ${ECLASS} (obsolete)"
		;;
	7)
		;;
	*)
		die "Unsupported EAPI=${EAPI} for ${ECLASS} (unknown)"
		;;
esac

if [[ ! ${_GIELLA_LANG} ]]; then

EXPORT_FUNCTIONS src_unpack src_prepare src_configure

# @ECLASS-VARIABLE: GIELLA_LANG
# @DESCRIPTION:
# ISO-639-3 language code for the language; defaults to PN, removing
# leading 'giella-'.
: ${GIELLA_LANG:=${PN#giella-}}

# @ECLASS-VARIABLE: GIELLA_EXP
# @REQUIRED
# @DESCRIPTION:
# Array of experiment classes; empty string is non-experimental.

local iuse="${GIELLA_EXP[@]}"
local iuse_defaults=0
for a in ${GIELLA_EXP[@]}; do
	[[ "$a" = *" "* ]] && die "Incorrect usage of experiment useflags; no spaces allowed in flag '$a'."
	if [[ -z "$a" ]] || [[ "$a" = "+"* ]]; then
		iuse_defaults+=1
	fi
done
if [[ $iuse_defaults -eq 0 ]]; then
	if [[ -n "${use# }" ]]; then
		iuse="+${iuse# }"
	fi
elif [[ $iuse_defaults -eq 1 ]]; then
	iuse="${iuse}"
else
	die "Too many default experiment useflags: $iuse."
fi

# @ECLASS-VARIABLE: GIELLA_EXP_IUSE
# @DESCRIPTION:
# Giella language experiment IUSE
readonly GIELLA_EXP_IUSE="$iuse"

unset iuse iuse_defaults

# @ECLASS-VARIABLE: GIELLA_EXP_REQUIRED_USE
# @DESCRIPTION:
# Giella language experiment REQUIRED_USE conditions.
if [[ -n "${GIELLA_EXP[@]}" ]]; then
	readonly GIELLA_EXP_REQUIRED_USE=" ?? ( ${GIELLA_EXP[@]//+/} )"
else
	readonly GIELLA_EXP_REQUIRED_USE=""
fi

REQUIRED_USE+="$GIELLA_EXP_REQUIRED_USE"

inherit autotools
if [[ ${PV} = *9999* ]]; then
	: ${EGIT_REPO_URI:=https://github.com/giellalt/lang-${GIELLA_LANG}}
	inherit git-r3
	: ${SRC_URI:=}
	: ${KEYWORDS:=}
else
	: ${SRC_URI:="https://github.com/giellalt/lang-${GIELLA_LANG}/archive/tags/v${PV}.tar.gz -> ${P}.tar.gz"}
	: ${KEYWORDS:=~amd64 ~arm ~x86}
fi

: ${HOMEPAGE:=https://giellatekno.uit.no}
IUSE+=" unoptimised-generators apertium morpher $GIELLA_EXP_IUSE"

# @ECLASS-VARIABLE: GIELLA_DEPEND
# @DESCRIPTION:
# List of build dependencies; should not be set by the ebuild.
readonly GIELLA_BDEPEND="sci-libs/giella-core
sci-libs/giella-shared
sci-misc/hfst"

# @ECLASS-VARIABLE: GIELLA_AMLIBS
# @DESCRIPTION:
# Source location of Giella core Automake libraries; read-only.
readonly GIELLA_AMLIBS=${EPREFIX}/usr/share/giella-core/am

# @ECLASS-VARIABLE: GIELLA_MYAMLIBS
# @DESCRIPTION:
# Source location of package-local Automake libraries; relative to $S.
if [[ "${PV}" = "9999" ]]; then
	: ${GIELLA_MYAMLIBS:=../giella-core/am-shared}
else
	: ${GIELLA_MYAMLIBS:=am-shared}
fi

# @FUNCTION: giella-lang-copy-amlibs
# @USAGE: 
# @DESCRIPTION:
# Copy Giella automake libraries from giella-core.
giella-lang-copy-amlibs() {
	local target="$S/${GIELLA_MYAMLIBS}"
	mkdir -p "$target"
	test -d "$target" || die "Unable to create GIELLA_MYAMLIBS=${GIELLA_MYAMLIBS}"
	test -d "${GIELLA_AMLIBS}" || die "Unable to locate GIELLA_AMLIBS=${GIELLA_AMLIBS}"
	einfo "Copying Giellatekno Automake libraries..."
	cp -r "$GIELLA_AMLIBS"/* "$target/" || die "Unable to copy Automake libs"
}

# @FUNCTION: giella-lang_src_unpack
# @USAGE:
# @DESCRIPTION:
# Unpack and copy in Giellatekno Automake libraries.
giella-lang_src_unpack() {
	if [[ ${PV} = *9999* ]]; then
		local fetched=
		for exp in ${GIELLA_EXP_IUSE//+/}; do
			if use $exp; then
				git-r3_fetch "${EGIT_REPO_URI}-x-$exp" || die "fetch failed"
				git-r3_checkout "${EGIT_REPO_URI}-x-$exp" || die "checkout failed"
				fetched=1
				break
			fi
		done
		[[ -n ${fetched} ]] || git-r3_src_unpack
		unset fetched
	else
		default
	fi
	giella-lang-copy-amlibs
}

# @FUNCTION: giella-lang-modify-buildsystem
# @USAGE:
# @DESCRIPTION:
# Update the build system to support Gentoo-specific features.
giella-lang-modify-buildsystem() {
	giella-lang-add-unoptimised-generators
}

# @FUNCTION: giella-lang-add-unoptimised-generators
# @USAGE:
# @DESCRIPTION:
# Add unoptimised generators as build and install targets.
giella-lang-add-unoptimised-generators() {
	generators="$(sed -n \
	'/^\s*GT_GENERATORS+=/,/^[^#].*[^\\]\(\s*#.*\)\?$/ {
		s/#.*$//; s/^if .*$//; s/^endif\( .*\)\?$//;
		/^\s*GT_GENERATORS+=/ { s/^\s*GT_GENERATORS+=//; };
		/\\$/! { H; x; s/\\//gm; s/^\s\+//gm; s/\s\+/\n/gm; p; s/^.*$//m; x }
	}'  "${GIELLA_MYAMLIBS}/src-dir-include.am" \
	| sort -u)"
	already_unoptimised="$(echo "$generators" | grep '\.hfst$' \
	| while read l; do
		eqawarn "Unconditionally installing unoptimised generator $l."
		echo "$l"
	done | sed 's/\.hfst$//' | tr '\n' '|' | sed 's/|$//; s/|/\\|/g')"
	if use unoptimised-generators; then
		must_deoptimise="$(echo "$generators" \
		| grep '\b\S*\.hfstol\b' \
		| grep -v '\b\('"$already_unoptimised"'\)\.hfstol\\b' \
		| sed 's/\.hfstol$//' \
		| while read l; do
			if find "${S}" "${GIELLA_MYAMLIBS}" -name '*.am' -exec \
			  sed '/^\s*GT_GENERATORS+=/,/^[^#].*[^\\]\(\s*#.*\)\?$/ {
				s/#.*$//; s/^if .*$//; s/^endif\( .*\)\?$//; p
			  }' {} \; 2>/dev/null \
			  | grep -q '\b'"$l"'\.hfst\b'; then
				eqawarn "Build system already installs unoptimised transducer ${l}.hfst"
			else
				echo "$l"
			fi
		done \
		| tr '\n' '|' | sed 's/|$//; s/|/\\|/g')"
		einfo "Adding unoptimised generators ${must_deoptimise//\\|/, }"
		sed '/^\s*GT_GENERATORS+=/,/^[^#].*[^\\]\(\s*#.*\)\?$/ {
			s:\b\('"$must_deoptimise"'\).hfstol\b:\0 \1.hfst:g
		}' \
			-i "${GIELLA_MYAMLIBS}/src-dir-include.am" \
		|| die "Unable to add unoptimised generators to build targets."
	fi
}

# @FUNCTION: giella-lang_src_prepare
# @USAGE:
# @DESCRIPTION:
# Perform build system modifications and generate configure scripts.
giella-lang_src_prepare() {
	giella-lang-modify-buildsystem
	default
	eautoreconf
}

# @FUNCTION: giella-lang-conf
# @USAGE: [additional econf options]
# @DESCRIPTION:
# Run econf with giella-lang use_enable's.
giella-lang-conf() {
	econf \
		"$(use_enable apertium)" \
		"$(use_enable morpher)"
}

# @FUNCTION: giella-lang_src_configure
# @USAGE: 
# @DESCRIPTION:
# Default src_configure for Giellatekno language packs; just runs
# giella-lang-conf.
giella-lang_src_configure() {
	giella-lang-conf
}


_GIELLA_LANG=1

fi

