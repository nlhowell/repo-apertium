# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit findlib

DESCRIPTION="An Ocaml library to deal with conll corpus files"
HOMEPAGE="https://gitlab.inria.fr/guillaum/libcaml-conll"

if [[ ${PV} = 9999 ]]; then
	SRC_URI=""
	EGIT_REPO_URI="https://gitlab.inria.fr/guillaum/libcaml-conll"
	inherit git-r3
else
	SRC_URI="https://opam.grew.fr/tgz/libcaml-conll.${PV}.tgz"
fi

LICENSE=""
SLOT="0"
KEYWORDS=""

DEPEND="dev-ml/yojson
dev-ml/ocaml-containers"
RDEPEND="${DEPEND}"
BDEPEND=""
IUSE="ocamlopt"

src_prepare() {
	default
	sed -i '/depends/,/\]/ { /".*"/ { /yojson/!d } }' opam
	eapply_user
}

src_install() {
	findlib_src_install
}
