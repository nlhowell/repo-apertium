# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit findlib

DESCRIPTION="A library with various log functions"
HOMEPAGE="https://gitlab.inria.fr/guillaum/libcaml-log"

if [[ ${PV} = 9999 ]]; then
	SRC_URI=""
	EGIT_REPO_URI="https://gitlab.inria.fr/guillaum/libcaml-log"
	inherit git-r3
else
	SRC_URI="https://opam.grew.fr/tgz/libcaml-log.${PV}.tgz"
fi

LICENSE=""
SLOT="0"
KEYWORDS=""

DEPEND="dev-ml/ANSITerminal"
RDEPEND="${DEPEND}"
BDEPEND=""
IUSE="ocamlopt"

src_install() {
	findlib_src_install
}
