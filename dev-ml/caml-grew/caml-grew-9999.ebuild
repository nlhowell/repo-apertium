# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit findlib

DESCRIPTION="Graph Rewriting for NLP"
HOMEPAGE="https://grew.fr"

if [[ ${PV} = 9999 ]]; then
	inherit git-r3
	SRC_URI=""
	EGIT_REPO_URI="https://gitlab.inria.fr/grew/libcaml-grew"
else
	SRC_URI=""
fi

LICENSE="CeCILL-2.1"
SLOT="0"
KEYWORDS="~arm ~amd64"

DEPEND="
dev-ml/caml-amr
dev-ml/caml-conll
dev-ml/caml-log
dev-ml/ocaml-containers
dev-ml/yojson
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	eapply "${FILESDIR}/grs-sandbox.patch"
	eapply_user
}
src_compile() {
	emake PREFIX=${EPREFIX}/usr
}
src_install() {
	findlib_src_install PREFIX=${EPREFIX}/usr
}
