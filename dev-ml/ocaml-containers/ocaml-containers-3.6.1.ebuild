# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DUNE_PKG_NAME=containers
inherit dune

DESCRIPTION="A modular, clean and powerful extension of the OCaml standard library"
HOMEPAGE="https://github.com/c-cube/ocaml-containers/"
SRC_URI="https://github.com/c-cube/ocaml-containers/archive/v${PV}.tar.gz"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~arm ~amd64"

DEPEND=">=dev-lang/ocaml-4.03.0
dev-ml/dune-configurator
dev-ml/either
dev-ml/seq
"
RDEPEND="${DEPEND}"
BDEPEND=""
IUSE="ocamlopt"
