# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit dune

DESCRIPTION="use the Either module defined in OCaml 4.12.0"
HOMEPAGE="https://github.com/mirage/either"
SRC_URI="https://github.com/mirage/either/releases/download/${PV}/${P}.tbz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~arm"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
IUSE="ocamlopt"
