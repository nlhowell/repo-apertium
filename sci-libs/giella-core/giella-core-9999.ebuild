# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

EGIT_REPO_URI="https://github.com/giellalt/${PN}"

inherit autotools git-r3

DESCRIPTION="Build and developer resources for GiellaLT"
HOMEPAGE="http://giellatekno.uit.no/"
SRC_URI=""

LICENSE="NOASSERTION"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
	eautoreconf
}
src_install() {
	default
	insinto /usr/share/${PN}/am
	doins am-shared/*
}
