<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<xsl:for-each select="supplementalData/languageData/language">
<xsl:value-of select="./@type" />:<xsl:value-of select="./@territories" />:<xsl:value-of select="./@scripts" />:
</xsl:for-each>
</xsl:template>

</xsl:stylesheet>
