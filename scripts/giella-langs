#!/bin/bash

d="$(readlink -f $(dirname "$0"))"
repo="$(readlink -f $d/..)"

repocache="$d/giellalt-repos.json"

function spdx_to_license()
{
  case "$1" in
  GPL-*.0)
    echo -n ${1%.0} ;;
  GPL-*.0+)
    echo -n ${1%.0+}+ ;;
  *)
    echo -n "$1" ;;
  esac
}

if [[ ! -f "$repocache" ]]; then
	lastpage="$(http https://api.github.com/orgs/giellalt/repos -h \
	| grep -i '^Link:' \
	| cut -d: -f2- \
	| tr , '\n' \
	| tr -d ' \r' \
	| awk -F';' '$2 == "rel=\"last\"" { print $1 } ' \
	| tr -d '<>' \
	| cut -d'?' -f2 \
	| tr '&' '\n' \
	| awk -F= '$1 == "page" { print $2 }'
	)"

	seq 1 $lastpage \
	| xargs -n1 -I@ http --print=b GET 'https://api.github.com/orgs/giellalt/repos?page=@' > "$repocache"
fi

ts="$(date '+%s')"

jq -r 'map(.name)|join("\n")' "$repocache" | grep ^lang- | sort \
| while read lang; do
	mod=${lang#lang-}
	lang=${mod%-x-*}
	exp=${mod#${lang}}
	exp=${exp#-x-}
	echo "LANG=$lang EXP=$exp"
	lang_name="$(jq -r <${EPREFIX}/usr/share/iso-codes/json/iso_639-3.json "
		.\"639-3\" | map(select(.\"alpha_3\" == \"$lang\")|.name)|first
	")"
	pn="giella-$lang"
	ebuild="$pn-9999.ebuild"
	pkgdir="$repo/app-dicts/$pn"
	mkdir -p "$pkgdir"
	if [[ -e "$pkgdir/$ebuild" ]]; then \
		if ! git cat-file -e HEAD:"${pkgdir#$repo/}/$ebuild" 2>/dev/null; then
			echo "WARNING: refusing to overwrite untracked file $pkgdir/$ebuild" >&2
			continue
		elif ! git diff --quiet "$pkgdir/$ebuild" && [[ "$(stat -c '%Y' $pkgdir/$ebuild)" -lt "$ts" ]]; then
			echo "WARNING: refusing to overwrite modified file$pkgdir/$ebuild" >&2
			continue
		fi
	fi
	if [[ ! -e "$pkgdir/$ebuild" ]] || [[ "$(stat -c '%Y' $pkgdir/$ebuild)" -lt "$ts" ]]; then
		lic="$(jq  -r <"$repocache" "
			map(select(.name == \"lang-$mod\"))|map(.license.spdx_id)|join(\"\")
		" | tr -d '\r\n' | sed 's/^ //')"
		lic="$(spdx_to_license "$lic")"
		desc_s="Giellatekno morphological dictionaries for $lang_name."
		desc_l="$(jq  -r <"$repocache" "
			map(select(.name == \"lang-$mod\"))|map(.description)|join(\"\")
		" | tr -d '\r\n' | sed 's/^ //')"
		sed "s%@LICENSE@%$lic%g;s%@DESCRIPTION@%$desc_s%g" "$d/giella-xxx-9999.ebuild" > "$pkgdir/$ebuild"
		sed "s%@DESCRIPTION@%$desc_l%g" "$d/giella-xxx-metadata.xml" > "$pkgdir/metadata.xml"
	fi
	sed -i '/GIELLA_EXP=/ { / "'"$exp"'" /!s/ )$/ "'"$exp"'" )/}' "$pkgdir/$ebuild"
done
