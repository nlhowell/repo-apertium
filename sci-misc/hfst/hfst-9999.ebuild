# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7
DISTUTILS_OPTIONAL=1
PYTHON_COMPAT=( python{3_8,3_9} )
inherit distutils-r1 autotools

if [[ ${PV} = 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/hfst/hfst"
else
	KEYWORDS="amd64 arm x86"
	SRC_URI="https://github.com/hfst/hfst/archive/tags/v${PV}.tar.gz -> ${P}.tar.gz"
	S="${WORKDIR}/${PN}-tags-v${PV}"
fi

DESCRIPTION="Helsinki Finite-State Technology libraries and tools"
HOMEPAGE="http://hfst.sf.net/"
LICENSE="GPL-2 GPL-3 Apache-2.0"
SLOT="0"
IUSE="minimal python"

REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"
COMMON_DEPEND="python? ( ${PYTHON_DEPS} )"
DEPEND=">=sys-devel/flex-2.5.35
>=sys-devel/bison-2.4
${COMMON_DEPEND}"
RDEPEND="${COMMON_DEPEND}"

src_prepare() {
	default
	eautoreconf
}
src_configure() {
	econf $(use_enable !minimal all-tools )
}

src_compile() {
	default_src_compile
	if use python ; then
		cd "${S}/python"
		mydistutilsargs=( build_ext )
		distutils-r1_src_compile
	fi
}

src_install() {
	default_src_install
	if use python ; then
		cd "${S}/python"
		distutils-r1_src_install
	fi
}
