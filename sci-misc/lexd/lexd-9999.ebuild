# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit autotools

DESCRIPTION="A lexicon compiler specialising in non-suffixational
morphologies."
HOMEPAGE="https://github.com/apertium/lexd"
if [[ "${PV}" = 9999 ]]; then
	EGIT_REPO_URI="https://github.com/apertium/${PN}"
	inherit git-r3
	KEYWORDS=""
else
	SRC_URI="https://github.com/apertium/lexd/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~x86"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE=""

RDEPEND=">=sci-misc/lttoolbox-3.5.2
dev-libs/icu"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

src_prepare() {
	default
	eautoreconf
}
