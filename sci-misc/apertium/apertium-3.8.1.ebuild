# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
EAPI=7

DISTUTILS_OPTIONAL=1
PYTHON_COMPAT=( python3_{8,9} )
inherit distutils-r1 autotools flag-o-matic

DESCRIPTION="Shallow-transfer machine Translation engine and toolbox"
HOMEPAGE="http://apertium.sourceforge.net/"
if [[ "${PV}" = 9999 ]]; then
	EGIT_REPO_URI="https://github.com/apertium/${PN}"
	inherit git-r3
	SRC_URI=""
	KEYWORDS=""
else
	SRC_URI="https://github.com/apertium/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	#SRC_URI="https://github.com/apertium/${PN}/releases/download/v${PV}/${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~x86"
fi

SLOT="0"
LICENSE="GPL-3+"

REQUIRED_USE="
	python? ( ${PYTHON_REQUIRED_USE} )
"
IUSE="python"

RDEPEND="
	>=dev-libs/libxml2-2.6.17
	dev-libs/libxslt
	>=sci-misc/lttoolbox-3.6.0
	>=dev-libs/icu-50
	dev-libs/utfcpp
	virtual/libiconv
	python? ( ${PYTHON_DEPS} )
"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

python_binding() {
	use python || return 0
	local ret=0
	pushd python || die
	"$@"
	ret=$?
	popd || die
	return $ret
}
src_prepare() {
	default
	eautoreconf
	python_binding distutils-r1_src_prepare
}
src_configure() {
	append-flags "-I/usr/include/utf8cpp"
	econf $(use_enable python python-bindings)
}
src_compile() {
	default
	python_binding distutils-r1_src_compile
}
src_install() {
	default
	use python && python_replicate_script "${D}"/usr/bin/apertium-{gen{v,vl,vr}dix,metalrx}
	python_binding distutils-r1_src_install
}
