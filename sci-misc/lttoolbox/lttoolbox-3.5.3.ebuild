# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit autotools

DESCRIPTION="Toolbox for lexical processing, morphological analysis and generation of words"
HOMEPAGE="http://apertium.sourceforge.net"
if [[ "${PV}" = 9999 ]]; then
	EGIT_REPO_URI="https://github.com/apertium/${PN}"
	inherit git-r3
	KEYWORDS=""
else
	SRC_URI="https://github.com/apertium/lttoolbox/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~x86"
fi

LICENSE="GPL-2"
SLOT="0"
IUSE="static-libs"

RDEPEND="dev-libs/libxml2:2"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

src_prepare() {
	default
	eautoreconf
}
src_configure() {
	econf $(use_enable static-libs static)
}
