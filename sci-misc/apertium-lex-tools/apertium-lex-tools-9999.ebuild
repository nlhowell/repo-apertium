# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

if [[ "${PV}" = 9999 ]]; then
	EGIT_REPO_URI="https://github.com/apertium/${PN}"
	inherit git-r3
	KEYWORDS=""
	SRC_URI=""
else
	SRC_URI="https://github.com/apertium/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~x86"
fi

DESCRIPTION="Lexical selection utilities for apertium toolchain"
HOMEPAGE="https://apertium.org"
LICENSE="GPL-2"
SLOT="0"
IUSE=""

COMMON_DEPEND=""
DEPEND="${COMMON_DEPEND}
	>=sci-misc/lttoolbox-3.5.3
	>=sci-misc/apertium-3.7.1
"
RDEPEND="${COMMON_DEPEND}"

src_prepare() {
	default
	eautoreconf
}
