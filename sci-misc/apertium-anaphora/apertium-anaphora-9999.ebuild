# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit cmake-utils

DESCRIPTION="Apertium anaphora resolution module."
HOMEPAGE="http://apertium.sourceforge.net/"
if [[ "${PV}" = 9999 ]]; then
	EGIT_REPO_URI="https://github.com/apertium/${PN}"
	inherit git-r3
	SRC_URI=""
	KEYWORDS=""
else
	SRC_URI="https://github.com/apertium/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~x86"
fi

SLOT="0"
LICENSE="GPL-3"

IUSE=""

RDEPEND="
	>=dev-libs/libxml2-2.6.17
	>=sci-misc/apertium-3.5.2
	>=sci-misc/lttoolbox-3.5.0
"
DEPEND="${RDEPEND}
	virtual/pkgconfig"
